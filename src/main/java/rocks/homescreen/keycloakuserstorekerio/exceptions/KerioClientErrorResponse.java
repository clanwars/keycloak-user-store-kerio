package rocks.homescreen.keycloakuserstorekerio.exceptions;

public class KerioClientErrorResponse extends Exception {
    private final Integer errorCode;

    public KerioClientErrorResponse(String message, Integer kerioErrorCode) {
        super(message);
        this.errorCode = kerioErrorCode;
    }

    public Integer getKerioErrorCode() {
        return errorCode;
    }
}
