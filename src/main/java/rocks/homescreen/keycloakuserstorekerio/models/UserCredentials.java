package rocks.homescreen.keycloakuserstorekerio.models;

import lombok.Data;

@Data
public class UserCredentials {
    private String userName;
    private String password;
    private Boolean passwordChanged = false;
}
