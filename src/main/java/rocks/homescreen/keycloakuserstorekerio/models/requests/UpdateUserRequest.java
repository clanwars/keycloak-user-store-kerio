package rocks.homescreen.keycloakuserstorekerio.models.requests;

import lombok.Data;
import rocks.homescreen.keycloakuserstorekerio.models.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class UpdateUserRequest {
    private String DomainId = "local";
    private List<UUID> UserIds = new ArrayList<>();
    private User Details;
}
