package rocks.homescreen.keycloakuserstorekerio.models.requests;

import lombok.Data;

@Data
public class FindGroupRequest {
    private String DomainId = "local";
    private GroupQuery Query = new GroupQuery();
}
