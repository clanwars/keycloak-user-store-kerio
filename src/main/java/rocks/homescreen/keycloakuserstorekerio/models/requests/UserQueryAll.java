package rocks.homescreen.keycloakuserstorekerio.models.requests;

import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Data
public class UserQueryAll {
    private int Start = 0;
    private int Limit = -1;
    private List<UserOrderByClause> OrderBy = new ArrayList<>(
            Collections.singletonList(new UserOrderByClause())
    );
}
