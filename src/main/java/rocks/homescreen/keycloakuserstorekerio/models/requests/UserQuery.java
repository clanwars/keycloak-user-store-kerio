package rocks.homescreen.keycloakuserstorekerio.models.requests;

import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Data
public class UserQuery {
    private int Start = 0;
    private int Limit = -1;
    private List<String> Fields = new ArrayList<>(Arrays.asList(
            "id",
            "credentials",
            "fullName",
            "email",
            "groups"
    ));
    private List<UserOrderByClause> OrderBy = new ArrayList<>(
            Collections.singletonList(new UserOrderByClause())
    );
    private List<FilterCondition> Conditions = new ArrayList<>();
    private String Combining;
}
