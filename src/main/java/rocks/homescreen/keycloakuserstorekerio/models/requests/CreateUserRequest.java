package rocks.homescreen.keycloakuserstorekerio.models.requests;

import lombok.Data;
import rocks.homescreen.keycloakuserstorekerio.models.User;

import java.util.ArrayList;
import java.util.List;

@Data
public class CreateUserRequest {
    private String DomainId = "local";
    private List<User> Users = new ArrayList<>();
}
