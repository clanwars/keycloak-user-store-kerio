package rocks.homescreen.keycloakuserstorekerio.models;

import lombok.Data;

import java.util.UUID;

@Data
public class Group {
    private UUID id;
    private String name;
    private String domainName;
    private Boolean isGroup;
}
