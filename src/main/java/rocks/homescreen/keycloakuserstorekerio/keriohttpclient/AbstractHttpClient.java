package rocks.homescreen.keycloakuserstorekerio.keriohttpclient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2Session;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;
import net.minidev.json.JSONObject;
import rocks.homescreen.keycloakuserstorekerio.exceptions.KerioClientErrorResponse;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

public abstract class AbstractHttpClient {
    private final JSONRPC2Session rpcSession;

    protected String serverAddress;
    protected String basePath;
    protected int requestId = 1;

    protected final ObjectMapper mapper = new ObjectMapper();

    public AbstractHttpClient(String serverAddress, String basePath) throws MalformedURLException {
        this.serverAddress = serverAddress;
        this.basePath = basePath;

        var serverURL = new URL(this.serverAddress + this.basePath);
        this.rpcSession = new JSONRPC2Session(serverURL);
        this.rpcSession.getOptions().acceptCookies(true);

        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    protected JSONObject sendJsonRpc2Request(String method, Object body) throws JSONRPC2SessionException, KerioClientErrorResponse {
        JSONRPC2Request request = new JSONRPC2Request(method, this.requestId++);
        Map<String, Object> result = mapper.convertValue(body, new TypeReference<Map<String, Object>>() {
        });
        request.setNamedParams(result);
        System.out.println(request.toString());
        var response = this.rpcSession.send(request);
        if (!response.indicatesSuccess()) {
            System.out.println(response.getError().toJSONObject());
            throw new KerioClientErrorResponse(response.getError().getMessage(), response.getError().getCode());
        }
        System.out.println(response.getResult());
        return (JSONObject) response.getResult();
    }

    protected void setCustomHeaders(Map<String, String> headers) {
        var cc = new CustomHttpHeaderHandler(headers);
        this.rpcSession.setConnectionConfigurator(cc);
    }
}
