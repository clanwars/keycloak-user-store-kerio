package rocks.homescreen.keycloakuserstorekerio.keriohttpclient;

import com.thetransactioncompany.jsonrpc2.client.ConnectionConfigurator;

import java.net.HttpURLConnection;
import java.util.Map;

public class CustomHttpHeaderHandler implements ConnectionConfigurator {

    private final Map<String, String> headers;

    public CustomHttpHeaderHandler(Map<String, String> headers) {
        this.headers = headers;
    }

    public void configure(HttpURLConnection connection) {

        // add custom HTTP header
        for (Map.Entry<String, String> entry : this.headers.entrySet()) {
            connection.addRequestProperty(entry.getKey(), entry.getValue());
        }
    }
}


