package rocks.homescreen.keycloakuserstorekerio.representations;

import org.keycloak.common.util.MultivaluedHashMap;
import org.keycloak.component.ComponentModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.storage.StorageId;
import org.keycloak.storage.adapter.AbstractUserAdapterFederatedStorage;
import rocks.homescreen.keycloakuserstorekerio.models.User;

import java.util.List;
import java.util.Map;

public class UserRepresentation extends AbstractUserAdapterFederatedStorage {
    private final User userEntity;

    public UserRepresentation(KeycloakSession session, RealmModel realm, ComponentModel storageProviderModel, User userEntity) {
        super(session, realm, storageProviderModel);
        this.userEntity = userEntity;

        if (userEntity.getFullName() == null) {
            return;
        }

        var fullName = userEntity.getFullName().split(",");
        if (fullName.length == 2) {
            this.setFirstName(fullName[1].trim());
            this.setLastName(fullName[0].trim());
        }
    }

    @Override
    public String getUsername() {
        return userEntity.getCredentials().getUserName();
    }

    @Override
    public void setUsername(String username) {
        userEntity.getCredentials().setUserName(username);
    }

    @Override
    public void setEmail(String email) {
        userEntity.setEmail(email);
    }

    @Override
    public String getEmail() {
        return userEntity.getEmail();
    }

    @Override
    public void setSingleAttribute(String name, String value) {
        super.setSingleAttribute(name, value);
    }

    @Override
    public void removeAttribute(String name) {
        super.removeAttribute(name);
    }

    @Override
    public void setAttribute(String name, List<String> values) {
        super.setAttribute(name, values);
    }

    @Override
    public String getFirstAttribute(String name) {
        return super.getFirstAttribute(name);
    }

    @Override
    public Map<String, List<String>> getAttributes() {
        Map<String, List<String>> attrs = super.getAttributes();
        MultivaluedHashMap<String, String> all = new MultivaluedHashMap<>();
        all.putAll(attrs);
        return all;
    }

    @Override
    public List<String> getAttribute(String name) {
        return super.getAttribute(name);
    }

    @Override
    public String getId() {
        return StorageId.keycloakId(storageProviderModel, userEntity.getId().toString());
    }

    public String getPassword() {
        return userEntity.getCredentials().getPassword();
    }

    public void setPassword(String password) {
        userEntity.getCredentials().setPassword(password);
    }
}
