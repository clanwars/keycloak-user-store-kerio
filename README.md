![](https://gitlab.com/homescreen.rocks/keycloak-user-store-kerio/badges/master/pipeline.svg)
# Keycloak User Storage SPI Kerio Implementation
This is a implementation of the Keycloak SPI API to connect to Kerio Control as a User Storage Provider.

Thanks go to https://github.com/nt-ca-aqe/keycloak-user-storage project which is used as a starting point for this implementation. 

## Setup

### Requirements
* A Kerio Control instance
* A Keycloak instance

### Getting Started

#### Steps to deploy the user storage provider
1. Execute `./gradlew jar` and wait till it's finished processing
2. Copy the `keycloak-user-store-X.X.X.jar` from your `./build/libs/`-folder to `<pathToKeycloak>/standalone/deployments/`
3. WildFly (= application server of keycloak) should now automatically deploy the JAR-file and make it available in Keycloak (Providing that keycloak is running)

#### Configure the user storage provider for your realm in keycloak
![alt text](img/keycloak-userstorage-config.png)
1. The name you want to be displayed by the user when it's a federated user. (and in the logs if something fails)
2. Url of the kerio admin interface
3. Username and password to the Kerio Admin interface

## Reference Documentation

For further reference, please consider the following sections:

* [Official Kerio Control Administration documentation](https://www.gfi.com/products-and-solutions/network-security-solutions/kerio-control/resources/developer-zone#developer-zone)
* [Official Keycloak SPI documentation](https://www.keycloak.org/docs/latest/server_development/index.html#_user-storage-spi)